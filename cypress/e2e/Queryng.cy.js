describe('template spec', () => {

    before(() => {
        cy.visit('https://example.cypress.io/commands/querying')
    });
    it('GET', () => {
        cy.get('#query-btn').should('contain', 'Button').click()
     
    })

    it('CONTAINS', () => {
        cy.get('.query-list')
           .contains('bananas').should('have.class', 'third')
        //  cy.get('.query-list')
        //     .contains(/^b\w+/).should('have.class', 'third')
        cy.get('.query-list')
            .contains('apples').should('have.class', 'first')
        
        cy.get('#querying')
            .contains('ul', 'oranges')
                .should('have.class', 'query-list')

        cy.get('.query-button')
            .contains('Save Form')
                .should('have.class', 'btn')
    });
    it('WITHIN', () => {
        cy.get('.query-form').within(() => {
            cy.get('input:first').should('have.attr', 'placeholder', 'Email')
            cy.get('input:last').should('have.attr', 'placeholder', 'Password')
        }) 
    });

    it.only('Root', () => {
        cy.get('.query-ul').within(() => {
            // In this within, the root is now the ul DOM element
            cy.root().should('have.class', 'query-ul')
          })
    });
  })

