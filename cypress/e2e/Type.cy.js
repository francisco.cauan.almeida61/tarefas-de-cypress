describe('type exercicios', () => {
    before(() => {
        cy.visit('https://example.cypress.io/commands/actions');
    });
    
    
    it('Passes', () => {
        cy.get('#email1') 
            .type('zepolvinho@gmail.com').should('have.value', 'zepolvinho@gmail.com')
                .type('{selectall}{del}')
                    .type('slowcabra@gmail.com', { delay : 300}).should('have.value', 'slowcabra@gmail.com')

    });
});